#!/usr/bin/env bash

# Read README file for background information

#default config vars:

INPUTFILE="/var/www/vhosts/h2827172.stratoserver.net/datafiles/notification_swap_file"
LOGFILE="/var/log/webserver_notification_ext"

if [[ -r $INPUTFILE && -w $INPUTFILE ]]
then
echo "File is readable, lets go:"
else
echo "File is not readable:"
fi


# Check if there is data to parse:
if [ -s ${INPUTFILE} ]
then
        echo "Start parsing, by dertermining the Notification service"
        echo "${INPUTFILE}"
        cat "${INPUTFILE}"
        cat ${INPUTFILE} | while read line
        do
                echo "$line"

                MSG_DATE=$(echo $line | awk -F "|" '{print $1}')
                MSG_SERVICE=$(echo $line | awk -F "|" '{print $2}')
                MSG_SRCFILE=$(echo $line | awk -F "|" '{print $3}')
                MSG_TITLE=$(echo $line | awk -F "|" '{print $4}')
                MSG_URL=$(echo $line | awk -F "|" '{print $5}')
                MSG_URLTITLE=$(echo $line | awk -F "|" '{print $6}')
                MSG_MESSAGE=$(echo $line | awk -F "|" '{print $7}')


                echo "Start service parser: $MSG_SERVICE"

# Parser for Pushover:
                if [ $MSG_SERVICE = "PUSHOVER" ]
                then
                        echo "Yesssss: $MSG_MESSAGE"
                        cd /opt/pushover.sh/ && ./pushover.sh -t $MSG_TITLE -u $MSG_URL -a $MSG_URLTITLE "$MSG_MESSAGE"


# Testphase for NEWLINE
#                       TEST="ATH \$\'\\n\' $'\n' bitcoin \r \n r n Newprice:"
#                       TEST1=$(cd /opt/pushover.sh/ && ./pushover.sh "${TEST}")
#                       echo "Combined: $TEST .  With $TEST1"
#                       $TEST1
#                       echo "test1: $TEST1"
#                       TEST2=$(cd /opt/pushover.sh/ && ./pushover.sh TEST2 ATH \$\'\\n\' $'\n' bitcoin \r \n r n Newprice)
#                       $TEST2
#                       echo "test2: ${TEST2}"
#
#
#                       echo "cd /opt/pushover.sh/ && ./pushover.sh $MSG_MESSAGE"
#                       echo "$MSG_MESSAGE"
                fi

        echo "PARSED Successfully: $line" >> $LOGFILE
        > $INPUTFILE
        done
else
        echo "Nothing to see..."
fi
